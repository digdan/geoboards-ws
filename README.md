# GeoBoard WS

**GeoBoard WS** is a live communication platform that uses websockets to create _boards_ or segmented areas of interest.

The idea is loosely based off of Yuku, which is a aggregation of online forums.

## Key Features

---

- Open Authentication (bitcoin like)
- Global profiles
- End to end encryption per _board_
- Live updates

## Code Layout

---
**Server** - This is the code responsible for the HTTP Upgrade and socket communication

**Sockets** - This is the code responsible for identifying and routing information to the correct connection.

**Reducer** - This is the code that is responsible for maintaining application state.

**Controllers** - This is the extendable plugin that allows for custom functionality. Which includes :

- **Authentication** - Provides mechanism for asserting an identification
- **Session Management** - Handles socket session information

## TODO

- Ability to create a board
- Ability for a user to subscribe to a board

## Overview

---
Geoboard is envisioned to be a hub of communication based around specific regions, topics, and areas of interest. The idea is that a user can _subscribe_ to a board and receive updates from other users based on that subscription. Think Twitter meets Reddit, but with extendable functionality.

## Future Considerations

---
Notes on future considerations.

  1. Users authenticate with their public key. This *IS* their account.
  2. Users can create other *identities* under their account. Activities and actions are based on their identities.
  3. A _board_ can be anything, and can have custom attributes.
  4. _Boards_ are moderated by democratic control. All users have the ability to *flag* an action on a post. Actions could include :
     1. Delete
     2. Mark as spam
     3. Offensive
     4. Off-topic
     5. Ban
     6. Move to other board
  5. Some boards can be _terminable_, where they are expected to come and go as things change. Example would be geographic centroids.
  6. _Boards_ can link to each other. Links are attributes
  7. _Boards_ sockets should be removed when no-one is connected.
  8. _Bots_ should have an easy API for integrating into Geoboards, but *must* present themselves as bots.

Boards can have features, which are given to them by the extendable modules functionality. These features can include :

  1. Live Wiki
  2. Chat room
  3. Forum
  4. Gallery
  5. Files
  6. User list, active/inactive

