import _ from 'lodash'


// Auth will have two bits of information. A token and a signature of the token
// To authenticate, see if the signed token equals the token in db
// The token is the sessionId
export const auth = (redis, data) => {
  return new Promise( (res, rej) => {
    redis.hmget(`auth:${data.user}`,'password').then( result => {
      if (result.pop() === data.password) {
        res(true);
      } else {
        res(false)
      }
    })
  })
}

export const signup = (redis, data) => {
  redis.hmset(`user:${data.user}`, {
    password:data.password,
    created:Date.now()
  })
  return true;
}