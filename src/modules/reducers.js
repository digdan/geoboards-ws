//NOT USED

import { socketsReducer } from './sockets'
import { sessionsReducer } from '../controllers/sessions'
import { authReducer } from '../controllers/auth'
import { auditReducer } from '../controllers/audit'

function reduceReducers(...reducers) {
  return (previous, current) =>
    reducers.reduce(
      (p, r) => r(p, current),
      previous
    );
}

export const reducers = (redis, sc) => {
  return reduceReducers(
    socketsReducer(redis, sc),
    sessionsReducer(redis, sc),
    authReducer(redis, sc),
    auditReducer(redis, sc)
  )
}
