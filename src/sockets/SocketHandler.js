import EventEmitter from 'events'
import uuidv4 from 'uuid/v4'
import uuidv5 from 'uuid/v5'
import _ from 'lodash'
import ws from 'ws'
import cookie from 'cookie'

const SOCKET_TIMEOUT = 60000; // 1 minute - time before closing socket
const SOCKET_REFRESH = 5000; // 5 second - ping interval

export const SOCKET_TIMEDOUT = '@socket/timedout'
export const SOCKET_PINGTABLE_UPDATE = '@socket/pingtable_update'
export const SOCKET_OPEN = '@socket/open'
export const SOCKET_CLOSE = '@socket/close'
export const SOCKET_DISCONNECT = '@socket/disconnect'
export const SOCKET_ERROR = '@socket/error'

export default class SocketHandler extends EventEmitter {
  constructor(sub) {
    super();
    this.socketMap = {};
    this.pingTable = {};
    this.channelMap = [];
    this.wss = new ws.Server({ noServer: true });
    this.sub = sub;
    this.initSocketEvents()
    this.initPinger()
    this.initSubHandler()
  }

  sockets() {
    return this.socketMap
  }

  pingTable() {
    return this.pingTable
  }

  disconnect(socketId, message) {
    this.sendSocket(socketId, {
      type: SOCKET_DISCONNECT,
      message
    }, () => {
      this.socketMap[socketId].terminate()
    })
  }

  sendSocket(socketId, message, cb) {
    if (_.has(this.socketMap, socketId)) {
      this.socketMap[socketId].send(JSON.stringify(message), cb)
    } else {
      console.log('sent to bad socketid', socketId)
    }
  }

  initSubHandler() {
    this.sub.on('message', (channel, message) => {
      const socketSet = _.get(this.channelMap, channel, new Set())
      socketSet.forEach( socketId => {
        this.sendSocket(socketId, JSON.parse(message))
      })
    })
  }

  unsubscribe(channel, socketId) {
    channel = `sub:${channel}`
    if (typeof socketId === 'undefined') {
      delete this.channelMap[channel]
      this.sub.unsubscribe(channel, err => {
        console.log('Error unsubscribing from channel', channel)
      })
      return
    } else {
      this.channelMap[channel].delete(socketId)
      if (this.channelMap[channel].size == 0) {
        return this.unsubscribe(channel)
      }
    }
  }
  subscribe(channel, socketId) {
    channel = `sub:${channel}`
    if (_.get(this.channelMap, channel, '') === '') {
      this.channelMap[channel] = new Set()
    }
    this.channelMap[channel].add(socketId)
    this.sub.subscribe(channel, err => {
      if (err) {
        console.log('Error subscribing to channel', channel);
      }
    })
  }

  initPinger() {
    setInterval( () => {
      this.wss.clients.forEach( ws => {
        if ((Date.now - SOCKET_TIMEOUT) > this.pingTable[ws.id]) {
          this.emit(SOCKET_TIMEDOUT, { socketId: ws.id })
          this.disconnect(ws.id, 'Client timed-out')
        }
        //ws.isAlive = false;
        ws.ping(()=>{});
      });
      this.emit(SOCKET_PINGTABLE_UPDATE, this.pingTable)
    }, SOCKET_REFRESH);
  }

  initSocketEvents() {
    this.wss.on('listening', () => {
      console.log('Websocket server listening on port', this.wss.options.port)
    });
    this.wss.on('connection', (incomingSocket, req) => {
      incomingSocket.id = uuidv5( uuidv4(), _.get(process.env.SERVER_ID, '1234567' ))
      this.pingTable[incomingSocket.id] = Date.now()
      this.emit(SOCKET_OPEN, {
        req,
        incomingSocket
      })
      incomingSocket.on('pong', () => {
        this.pingTable[incomingSocket.id] = Date.now()
      })
      incomingSocket.on('close', () => {
        delete this.pingTable[incomingSocket.id]
        delete this.socketMap[incomingSocket.id]
        this.channelMap.forEach( channel => {
          channel.delete(incomingSocket.id)
        })
        this.emit(SOCKET_CLOSE, {
          req,
          incomingSocket
        })
      })
      incomingSocket.on('message', message => {
        const messageData = JSON.parse(message)
        const channel = messageData.type
        if (channel.indexOf('@') == 0) { //Only system calls start with '@'
          return
        }
        delete messageData.type
        messageData.socketId = incomingSocket.id
        if (this.listenerCount(channel) === 0) {
          this.sendSocket(incomingSocket.id, {
            type: 'error/invalid_command'
          })
        } else {
          this.emit(channel, messageData)
        }
      })
      this.socketMap[incomingSocket.id] = incomingSocket
      //subscribe
      this.subscribe(`socket:${incomingSocket.id}`, incomingSocket.id)
    })
  }
  
  verifyClient(info, next) {
    const hash = crypto.createHash('sha256')
    let IPAddress = _.get(info.req.headers,'x-real-ip', _.get(info.req.headers,'x-forwarded-for', '0.0.0.0'))
    const cookies = cookie.parse(_.get(info.req.headers,'cookie',''));
    const socketInfo = {
      sessionId: _.get(cookies,'gs', hash.update(IPAddress).digest('hex')),
      created: Date.now(),
      IPAddress
    }
    info.req.socketData = socketInfo
    if (socketInfo.sessionId === '') {
      console.log('Rejecting unidentifiable connection')
    }
    next(socketInfo.sessionId !== '')
  }
}
