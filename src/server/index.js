import http from 'http';
import sessionStart from '../sessions';
import auth from '../auth';

class Server {
  constructor({port, socketHandler, db }, cb) {
    this.http = http.createServer(this.requestListener);
    this.http.listen(port, cb);
    this.db = db;
    this.socketHandler = socketHandler;
  }
  requestListener = (req, res) => {
    req.on('error', err => {
      console.error(err);
      // Handle error...
      res.statusCode = 400;
      res.end('400: Bad Request');
      return;
    });
    if (req.url === '/connect') {
      this.socketHandler.wss.handleUpgrade(req, req.socket, Buffer.alloc(0), this.socketHandler.verifySocket)
    } else {
      if (req.options.method === 'POST') {
        auth(db, res).then( result => {
          sessionStart(db, result);
        }).catch( err => {
          res.writeHead(403);
          res.end('Bad auth!');  
        });  
      } else if (req.options.method === 'GET') {
        //TODO - build token
        // A get should have the public key of the user
        // Public key should sign the token
        // Then post the signed token ( from client )
      }
    }
  }
}

export {
  Server as default
}