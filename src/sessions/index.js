import _ from 'lodash'

const SOCKET_OPEN = 1;
const SOCKET_CLOSE = 0;

const SESSION_TIMEOUT = 1000 * 60 * 5 // 5 minutes

//Session action types
const SESSION_STATUS = 'session/status'

const sessions = (sc, db) => {
  const sessionStatus = data => {
    //session data
    db.cache.hgetall(`session:${data.sessionId}`, (err, results) => {
      results.type = SESSION_STATUS
      db.publish(`socket:${data.socketId}`, results);
    })
  }

  sc.on(SOCKET_OPEN, data => {
    db.cache.sadd(`sessionSockets:${data.req.socketData.sessionId}`, data.incomingSocket.id)
    db.cache.hset(`socket:${data.incomingSocket.id}`,[
      'sessionId', data.req.socketData.sessionId,
      'socketStart', Date.now()
    ], () => {
      sc.subscribe(`session:${data.req.socketData.sessionId}`, data.incomingSocket.id)
      sessionStatus({
        socketId: data.incomingSocket.id
      });
    });
  })
  sc.on(SOCKET_CLOSE, data => {
    db.cache.srem('sockets', data.incomingSocket.id);
    db.cache.del(`socket:${data.incomingSocket.id}`);
    db.cache.srem(`sessionSockets:${data.req.socketData.sessionId}`, data.incomingSocket.id);
    db.cache.scard(`sessionSocket:${data.req.socket.sessionId}`, (err, results) => {
      if (results === 0) { //No sockets in session
        db.cache.del(`sessionSocket:${data.req.socket.sessionId}`);
        db.cache.pexpire(`session:${data.rqe.socketData.sessionId}`, SESSION_TIMEOUT);
      }
    });
  })
  sc.on(SESSION_STATUS, data => {
    sessionStatus(data);
  })
}

const sessionStart = (db, req) => {
  return new Promise( (resolve, reject) => {
    //TODO build session from http auth
    console.log('Store session token');
    resolve();
  });
}

export {
  sessions as default,
  sessionStart
}
