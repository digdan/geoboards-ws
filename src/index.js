import Server from './server';
import SocketHandler from './sockets/SocketHandler'
import Redis from 'ioredis'
import _ from 'lodash'
import controllers from './controllers'

const port = process.env.PORT || 80

let db = { //db-layer
  cache: new Redis( process.env.REDIS_URL, {keyPrefix: 'cache:'} ),
  store: new Redis( process.env.REDIS_URL )
}
db.publish = (channel, message) => {  
  db.store.publish(`sub:${channel}`, JSON.stringify(message))
}

let sub = new Redis( process.env.REDIS_URL )

const socketHandler = new SocketHandler(sub);
const server = new Server(
  {
    port,
    socketHandler,
    db
  },
  () => {
    console.log('Webserver listening on port', port);
  }
);

//Bootstrap Controllers
_.forOwn(controllers, controller => {
  controller(socketHandler, db)
})
