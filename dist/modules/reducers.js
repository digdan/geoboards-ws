'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reducers = undefined;

var _sockets = require('./sockets');

var _sessions = require('../controllers/sessions');

var _auth = require('../controllers/auth');

var _audit = require('../controllers/audit');

//NOT USED

function reduceReducers() {
  for (var _len = arguments.length, reducers = Array(_len), _key = 0; _key < _len; _key++) {
    reducers[_key] = arguments[_key];
  }

  return function (previous, current) {
    return reducers.reduce(function (p, r) {
      return r(p, current);
    }, previous);
  };
}

var reducers = exports.reducers = function reducers(redis, sc) {
  return reduceReducers((0, _sockets.socketsReducer)(redis, sc), (0, _sessions.sessionsReducer)(redis, sc), (0, _auth.authReducer)(redis, sc), (0, _audit.auditReducer)(redis, sc));
};