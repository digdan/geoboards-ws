'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SOCKET_ERROR = exports.SOCKET_DISCONNECT = exports.SOCKET_CLOSE = exports.SOCKET_OPEN = exports.SOCKET_PINGTABLE_UPDATE = exports.SOCKET_TIMEDOUT = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _v3 = require('uuid/v5');

var _v4 = _interopRequireDefault(_v3);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ws = require('ws');

var _ws2 = _interopRequireDefault(_ws);

var _cookie = require('cookie');

var _cookie2 = _interopRequireDefault(_cookie);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SOCKET_TIMEOUT = 60000; // 1 minute - time before closing socket
var SOCKET_REFRESH = 5000; // 5 second - ping interval

var SOCKET_TIMEDOUT = exports.SOCKET_TIMEDOUT = '@socket/timedout';
var SOCKET_PINGTABLE_UPDATE = exports.SOCKET_PINGTABLE_UPDATE = '@socket/pingtable_update';
var SOCKET_OPEN = exports.SOCKET_OPEN = '@socket/open';
var SOCKET_CLOSE = exports.SOCKET_CLOSE = '@socket/close';
var SOCKET_DISCONNECT = exports.SOCKET_DISCONNECT = '@socket/disconnect';
var SOCKET_ERROR = exports.SOCKET_ERROR = '@socket/error';

var SocketConnect = function (_EventEmitter) {
  _inherits(SocketConnect, _EventEmitter);

  function SocketConnect(sub) {
    _classCallCheck(this, SocketConnect);

    var _this = _possibleConstructorReturn(this, (SocketConnect.__proto__ || Object.getPrototypeOf(SocketConnect)).call(this));

    _this.socketMap = {};
    _this.pingTable = {};
    _this.channelMap = [];
    _this.sub = sub;
    return _this;
  }

  _createClass(SocketConnect, [{
    key: 'startServer',
    value: function startServer() {
      var port = process.env.PORT || 80;
      var verifyClient = this.verifyClient;
      this.wss = new _ws2.default.Server({
        port: port,
        verifyClient: verifyClient,
        clientTracking: true
      });
      this.initSocketEvents();
      this.initPinger();
      this.initSubHandler();
    }
  }, {
    key: 'sockets',
    value: function sockets() {
      return this.socketMap;
    }
  }, {
    key: 'pingTable',
    value: function pingTable() {
      return this.pingTable;
    }
  }, {
    key: 'disconnect',
    value: function disconnect(socketId, message) {
      var _this2 = this;

      this.sendSocket(socketId, {
        type: SOCKET_DISCONNECT,
        message: message
      }, function () {
        _this2.socketMap[socketId].terminate();
      });
    }
  }, {
    key: 'sendSocket',
    value: function sendSocket(socketId, message, cb) {
      if (_lodash2.default.has(this.socketMap, socketId)) {
        this.socketMap[socketId].send(JSON.stringify(message), cb);
      } else {
        console.log('sent to bad socketid', socketId);
      }
    }
  }, {
    key: 'initSubHandler',
    value: function initSubHandler() {
      var _this3 = this;

      this.sub.on('message', function (channel, message) {
        var socketSet = _lodash2.default.get(_this3.channelMap, channel, new Set());
        socketSet.forEach(function (socketId) {
          _this3.sendSocket(socketId, JSON.parse(message));
        });
      });
    }
  }, {
    key: 'unsubscribe',
    value: function unsubscribe(channel, socketId) {
      channel = 'sub:' + channel;
      if (typeof socketId === 'undefined') {
        delete this.channelMap[channel];
        this.sub.unsubscribe(channel, function (err) {
          console.log('Error unsubscribing from channel', channel);
        });
        return;
      } else {
        this.channelMap[channel].delete(socketId);
        if (this.channelMap[channel].size == 0) {
          return this.unsubscribe(channel);
        }
      }
    }
  }, {
    key: 'subscribe',
    value: function subscribe(channel, socketId) {
      channel = 'sub:' + channel;
      if (_lodash2.default.get(this.channelMap, channel, '') === '') {
        this.channelMap[channel] = new Set();
      }
      this.channelMap[channel].add(socketId);
      this.sub.subscribe(channel, function (err) {
        if (err) {
          console.log('Error subscribing to channel', channel);
        }
      });
    }
  }, {
    key: 'initPinger',
    value: function initPinger() {
      var _this4 = this;

      setInterval(function () {
        _this4.wss.clients.forEach(function (ws) {
          if (Date.now - SOCKET_TIMEOUT > _this4.pingTable[ws.id]) {
            _this4.emit(SOCKET_TIMEDOUT, { socketId: ws.id });
            _this4.disconnect(ws.id, 'Client timed-out');
          }
          //ws.isAlive = false;
          ws.ping(function () {});
        });
        _this4.emit(SOCKET_PINGTABLE_UPDATE, _this4.pingTable);
      }, SOCKET_REFRESH);
    }
  }, {
    key: 'initSocketEvents',
    value: function initSocketEvents() {
      var _this5 = this;

      this.wss.on('listening', function () {
        console.log('Websocket server listening on port', _this5.wss.options.port);
      });
      this.wss.on('connection', function (incomingSocket, req) {
        incomingSocket.id = (0, _v4.default)((0, _v2.default)(), process.env.SERVER_ID);
        _this5.pingTable[incomingSocket.id] = Date.now();
        _this5.emit(SOCKET_OPEN, {
          req: req,
          incomingSocket: incomingSocket
        });
        incomingSocket.on('pong', function () {
          _this5.pingTable[incomingSocket.id] = Date.now();
        });
        incomingSocket.on('close', function () {
          delete _this5.pingTable[incomingSocket.id];
          delete _this5.socketMap[incomingSocket.id];
          _this5.channelMap.forEach(function (channel) {
            channel.delete(incomingSocket.id);
          });
          _this5.emit(SOCKET_CLOSE, {
            req: req,
            incomingSocket: incomingSocket
          });
        });
        incomingSocket.on('message', function (message) {
          var messageData = JSON.parse(message);
          var channel = messageData.type;
          if (channel.indexOf('@') == 0) {
            //Only system calls start with '@'
            return;
          }
          delete messageData.type;
          messageData.socketId = incomingSocket.id;
          if (_this5.listenerCount(channel) === 0) {
            _this5.sendSocket(incomingSocket.id, {
              type: 'error/invalid_command'
            });
          } else {
            _this5.emit(channel, messageData);
          }
        });
        _this5.socketMap[incomingSocket.id] = incomingSocket;
        //subscribe
        _this5.subscribe('socket:' + incomingSocket.id, incomingSocket.id);
      });
    }
  }, {
    key: 'verifyClient',
    value: function verifyClient(info, next) {
      var hash = _crypto2.default.createHash('sha256');
      var IPAddress = _lodash2.default.get(info.req.headers, 'x-real-ip', _lodash2.default.get(info.req.headers, 'x-forwarded-for', '0.0.0.0'));
      var cookies = _cookie2.default.parse(_lodash2.default.get(info.req.headers, 'cookie', ''));
      var socketInfo = {
        sessionId: _lodash2.default.get(cookies, 'gs', hash.update(IPAddress).digest('hex')),
        created: Date.now(),
        IPAddress: IPAddress
      };
      info.req.socketData = socketInfo;
      if (socketInfo.sessionId === '') {
        console.log('Rejecting unidenfiable connection');
      }
      next(socketInfo.sessionId !== '');
    }
  }]);

  return SocketConnect;
}(_events2.default);

exports.default = SocketConnect;