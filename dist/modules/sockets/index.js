'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sockets = undefined;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ws = require('ws');

var _ws2 = _interopRequireDefault(_ws);

var _verifyClient = require('./verifyClient');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Config Constants
var SOCKET_TIMEOUT = 60000; // 1 minute - time before closing socket
var SOCKET_REFRESH = 5000; // 5 second - ping interval

var startServer = function startServer() {
  return new Promise(function (res, rej) {
    var port = process.env.PORT || 80;
    var wss = new _ws2.default.Server({
      port: port,
      verifyClient: _verifyClient.verifyClient,
      clientTracking: true
    });
    setInterval(function () {
      wss.clients.forEach(function (ws) {
        if (ws.isAlive - SOCKET_TIMEOUT < Date.now()) {
          ws.terminate();
        }
        //ws.isAlive = false;
        ws.ping(function () {});
      });
    }, SOCKET_REFRESH);
    wss.on('listening', function () {
      console.log('Websocket server listening on port', wss.options.port);
      res(wss);
    });
  });
};

var sockets = exports.sockets = function sockets() {
  return startServer();
};