'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.verifyClient = undefined;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _cookie = require('cookie');

var _cookie2 = _interopRequireDefault(_cookie);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var crypto = require('crypto');
var hash = crypto.createHash('sha256');

var verifyClient = exports.verifyClient = function verifyClient(info, next) {
  var IPAddress = _lodash2.default.get(info.req.headers, 'x-real-ip', _lodash2.default.get(info.req.headers, 'x-forwarded-for', '0.0.0.0'));
  var cookies = _cookie2.default.parse(_lodash2.default.get(info.req.headers, 'cookie', ''));
  var socketInfo = {
    clientKey: _lodash2.default.get(cookies, 'gs', hash.update(IPAddress).digest('hex')),
    created: Date.now(),
    IPAddress: IPAddress
  };
  info.req.socketData = socketInfo;
  if (socketInfo.clientKey === '') {
    console.log('Rejecting unidenfiable connection');
  }
  next(socketInfo.clientKey !== '');
};