'use strict';

var _SocketConnect = require('./modules/SocketConnect');

var _SocketConnect2 = _interopRequireDefault(_SocketConnect);

var _ioredis = require('ioredis');

var _ioredis2 = _interopRequireDefault(_ioredis);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _controllers = require('./controllers');

var _controllers2 = _interopRequireDefault(_controllers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var db = { //db-layer
  cache: new _ioredis2.default(process.env.REDIS_URL, { keyPrefix: 'cache:' }),
  store: new _ioredis2.default(process.env.REDIS_URL)
};
db.publish = function (channel, message) {
  db.store.publish('sub:' + channel, JSON.stringify(message));
};

var sub = new _ioredis2.default(process.env.REDIS_URL);

//Sockets Handler
var sc = new _SocketConnect2.default(sub);

//Bootstrap Controllers
_lodash2.default.forOwn(_controllers2.default, function (controller) {
  controller(sc, db);
});

//Start Server
sc.startServer();
console.log('Websocket server started');