'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.authReducer = exports.signup = exports.authenticate = exports.AUTH_LOGINFAILED = exports.AUTH_LOGINSUCCESS = exports.AUTH_SIGNEDUP = exports.AUTH_LOGGEDIN = exports.AUTH_SIGNUP = exports.AUTH = undefined;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Authentication response action types
var AUTH = exports.AUTH = 'auth';
var AUTH_SIGNUP = exports.AUTH_SIGNUP = 'auth/signup';
var AUTH_LOGGEDIN = exports.AUTH_LOGGEDIN = 'auth/loggedin';
var AUTH_SIGNEDUP = exports.AUTH_SIGNEDUP = 'auth/signedup';
var AUTH_LOGINSUCCESS = exports.AUTH_LOGINSUCCESS = 'auth/login/success';
var AUTH_LOGINFAILED = exports.AUTH_LOGINFAILED = 'auth/login/failed';

var authenticate = exports.authenticate = function authenticate(redis, data) {
  return new Promise(function (res, rej) {
    redis.hmget('auth:' + data.user, 'password').then(function (result) {
      if (result.pop() === data.password) {
        res(true);
      } else {
        res(false);
      }
    });
  });
};

var signup = exports.signup = function signup(redis, data) {
  redis.hmset('user:' + data.user, {
    password: data.password,
    created: Date.now()
  });
  return true;
};

var authReducer = exports.authReducer = function authReducer(sc, db) {
  //Event controller ( handle events asynchronously, then dispatch results)
  sc.on(AUTH, function (event) {
    authenticate(db, event.data).then(function (res) {
      if (res === true) {
        console.log('authdata', event);
        sc.dispatch({
          type: AUTH_LOGINSUCCESS,
          data: event.data
        });
      } else {
        sc.send('socket', event.socketId, { type: AUTH_LOGINFAILED });
      }
    });
  });

  sc.on(AUTH_SIGNUP, function (event) {
    signup(redis, event.data).then(function (res) {
      send('socket', event.socketId, { type: AUTH_SIGNEDUP });
    });
  });

  return function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var action = arguments[1];

    var newState = _lodash2.default.cloneDeep(state);
    switch (action.type) {
      case AUTH_LOGINSUCCESS:
        {
          send('socket', action.data.socketId, { type: SESSION_STATUS, data: getSession(newState, action.data.socketId) });
          return newState;
        }
      case AUTH_SIGNEDUP:
        {
          return newState;
        }
      default:
        {
          return newState;
        }
    }
  };
};