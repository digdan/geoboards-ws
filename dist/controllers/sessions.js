'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _SocketConnect = require('../modules/SocketConnect');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SESSION_TIMEOUT = 1000 * 60 * 5; // 5 minutes

//Session action types
var SESSION_STATUS = 'session/status';

var sessions = function sessions(sc, db) {
  var sessionStatus = function sessionStatus(data) {
    //session data
    db.cache.hgetall('session:' + data.sessionId, function (err, results) {
      results.type = SESSION_STATUS;
      db.publish('socket:' + data.socketId, results);
    });
  };

  sc.on(_SocketConnect.SOCKET_OPEN, function (data) {
    db.cache.sadd('sessionSockets:' + data.req.socketData.sessionId, data.incomingSocket.id);
    db.cache.hset('socket:' + data.incomingSocket.id, ['sessionId', data.req.socketData.sessionId, 'socketStart', Date.now()], function () {
      sc.subscribe('session:' + data.req.socketData.sessionId, data.incomingSocket.id);
      sessionStatus({
        socketId: data.incomingSocket.id
      });
    });
  });
  sc.on(_SocketConnect.SOCKET_CLOSE, function (data) {
    db.cache.srem('sockets', data.incomingSocket.id);
    db.cache.del('socket:' + data.incomingSocket.id);
    db.cache.srem('sessionSockets:' + data.req.socketData.sessionId, data.incomingSocket.id);
    db.cache.scard('sessionSocket:' + data.req.socket.sessionId, function (err, results) {
      if (results === 0) {
        //No sockets in session
        db.cache.del('sessionSocket:' + data.req.socket.sessionId);
        db.cache.pexpire('session:' + data.rqe.socketData.sessionId, SESSION_TIMEOUT);
      }
    });
  });
  sc.on(SESSION_STATUS, function (data) {
    sessionStatus(data);
  });
};

exports.default = sessions;